from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    is_published = models.BooleanField(default=False)

from django import forms
from recipes.models import Recipe


class RecipeForm(forms.ModelForm):
    email_address = forms.EmailField(max_length=300)

    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]
