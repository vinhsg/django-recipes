# Generated by Django 4.2 on 2023-04-06 18:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recipe',
            old_name='create_on',
            new_name='created_on',
        ),
    ]
