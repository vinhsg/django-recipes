from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


def create_post(request):
    if request.method == "POST":
        form = PostForm(request.POST) # POST is when the person has submitted the form
        if form.is_valis():# We should use the form to validate the values
            form.save()#   and save them to the database
            return redirect("posts_list")# If all goes well, we can redirect the browser
        #   to another page and leave the function
    else:
        form = PostForm()# Create an instance of the Django model form class
    context = {
        "post_form": form,
      }
    return render(request, "posts/create.html", context)
    # Put the form in the context
    # Render the HTML template with the form


def create_recipe(request):
    if request.method == "POST":
      form = RecipeForm(request.POST)
      if form.is_valid():
          form.save()
          return redirect("recipe_list")
    else:
        form = RecipeForm()

    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)


def edit_recipe(request, pk):
    if request.method == 'POST':
        form = <somthing>s(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = MyModelForm(instance=mymodel)

    context = {
        'form': form,
    }
    return render(request, "recipes/edit.html", context)
